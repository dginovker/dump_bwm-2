==================================================================
Wettelijke informatie
==================================================================

DEZE SOFTWARE WORDT GELEVERD ZONDER GARANTIES, VOORWAARDEN OF
VERKLARINGEN, RECHTSTREEKS OF IMPLICIET,WAARONDER OOK DE VERHANDELBAARHEID
OF GESCHIKTHEID VOOR EEN BEPAALD DOEL. ELECTRONIC ARTS KAN IN GEEN
ENKEL GEVAL VERANTWOORDELIJK WORDEN GESTELD VOOR ENIGE DIRECTE OF
INDIRECTE SCHADE, AL DAN NIET PER ONGELUK, OF SCHADE VOORTVLOEIEND
UIT HET GEBRUIK, BEZIT, OF MISBRUIK VAN DIT ELECTRONIC ARTS SOFTWARE PRODUCT.

SOMMIGE LANDEN STAAN GEEN BEPERKINGEN TOE ALS HET GAAT OM DE DUUR VAN
EEN GARANTIE EN/OF UITSLUITINGEN OF BEPERKINGEN VAN GELEDEN SCHADE, AL
DAN NIET PER ONGELUK, DUS BOVENSTAANDE UITSLUITINGEN EN/OF BEPERKINGEN
VAN DE WETTELIJKE AANSPRAKELIJKHEID ZIJN MOGELIJK NIET OP JOUW SITUATIE
VAN TOEPASSING. DEZE GARANTIE VERLEENT DE CONSUMENT BEPAALDE RECHTEN, 
DIE NAAST DE GRONDWETTELIJKE RECHTEN VAN DE CONSUMENT STAAN. DEZE RECHTEN
KUNNEN PER LAND VERSCHILLEN.

ELECTRONIC ARTS BEHOUDT ZICH HET RECHT VOOR VERBETERINGEN AAN TE
BRENGEN AAN DIT BESTAND EN DE SOFTWARE, OP ELK GEWENST MOMENT EN ZONDER
MELDING VOORAF. OP DIT BESTAND EN DE SOFTWARE DIE WORDT BESCHREVEN IN DIT
BESTAND BERUST COPYRIGHT. ALLE RECHTEN VOORBEHOUDEN. GEEN ENKEL DEEL VAN
DEZE HANDLEIDING OF DE DAARIN BESCHREVEN SOFTWARE MAG GEKOPIEERD, 
GEREPRODUCEERD, VERTAALD OF OMGEZET WORDEN NAAR EEN ELEKTRONISCH MEDIUM
OF MECHANISCH LEESBARE VORM ZONDER VOORAFGAANDE SCHRIFTELIJKE TOESTEMMING
VAN ELECTRONIC ARTS.


==================================================================
Auteursrecht Electronic Arts
==================================================================

Software en documentatie (c) 2004 Electronic Arts Inc.

ALLE RECHTEN VOORBEHOUDEN.


==================================================================
The Lord of the Rings(tm), The Battle for Middle-earth(tm) 
LeesMij-bestand versie 1.0 

11 november 2004
==================================================================

Bedankt voor het aanschaffen van The Battle for Middle-earth(tm). 
Dit LeesMij-bestand bevat informatie waarmee je je reis door
Midden-aarde kunt beginnen.

==================================================================
Systeemvereisten
==================================================================

DirectX 9.0b is vereist om The Battle for Middle-earth(tm) te kunnen
spelen. Ga naar de volgende site om DirectX 9.0b of hoger te
installeren:

  http://www.microsoft.com/windows/directx/default.aspx

MINIMALE CONFIGURATIE:

  - Windows XP of Windows 2000
  - 1,3 GHz Intel Pentium IV- of AMD Athlon-processor
  - 256 MB RAM
  - Nvidia GeForce 2 of gelijkwaardige grafische kaart met 32 MB RAM
  - DirectX 9.0b compatibele geluidskaart
  - 4 GB vrije harde-schijfruimte

Onthoud dat je The Battle for Middle-earth(tm) kunt spelen met
bovenstaande specificaties, maar dat extra geheugen (bovenop de
vereiste 256 MB RAM) wordt aangeraden om het spel soepeler te laten
draaien.

Nvidia GeForce 2 MX wordt niet ondersteund. 

==================================================================
Installatieinstructies
==================================================================

Controleer voordat je The Battle for Middle-earth(tm) start, of
de nieuwste stuurprogramma's (drivers) voor je grafische kaart en
geluidskaart zijn ge�nstalleerd. Je kunt de betreffende stuurprogramma's
downloaden van de website van de fabrikant van je grafische kaart en
geluidskaart.

Zorg ervoor dat er geen andere programma's draaien op de achtergrond,
want dit kan de prestatie van het spel be�nvloeden. Onder deze programma's
worden ook antivirusprogramma's verstaan die op de achtergrond worden
uitgevoerd. Ook deze kunnen de prestatie van het spel be�nvloeden.

The Battle for Middle-earth(tm)installeren:

  1) Plaats de dvd van The Battle for Middle-earth(tm) in je
dvd-romspeler.

     Als de dvd niet automatisch opstart als je hem in de speler plaatst,
is deze optie mogelijk uitgeschakeld. Raadpleeg de gebruikershandleiding
van Windows om de functie 'Bericht bij automatisch invoegen' aan te
zetten.

     Als je de functie 'Bericht bij automatisch invoegen' niet kunt of wilt
gebruiken, kun je met behulp van de Windows Verkenner bladeren naar de locatie
van de The Battle for Middle-earth(tm) dvd-romspeler. Dubbelklik op
'Setup' in de root van de map.


  2) Volg de instructies die in beeld verschijnen om The Battle for
     Middle-earth(tm) te installeren.

  3) Onthoud dat je The Battle for Middle-earth(tm) tijdens de installatie
     moet registreren als daarom wordt gevraagd, om online te kunnen spelen. 
     Je kunt het spel ook later registreren, door in het menu Online te klikken
     op Registreren.

Als je problemen ondervindt tijdens het installeren van The Battle for
Middle-earth(tm), of andere technische problemen ondervindt,klik dan op de knop
'Hulp' in het automatische opstartmenu van het spel. Je kunt ook een bezoek
brengen aan het onderdeel 'Support'op de offici�le product-website
(zie hieronder). Je moet je spel registreren om in aanmerking te komen voor
volledige klantenservice.

http://www.eagames.com//official/lordoftherings/thebattleformiddleearth/uk/


==================================================================
The Battle for Middle-earth(tm) verwijderen
==================================================================

  Zo kun je The Battle for Middle-earth(tm) verwijderen:

  1) Open het menu 'Start'.
  2) Kies 'Instellingen'.
  3) Kies 'Configuratiescherm'.
  4) Kies 'Software'.
  5) Selecteer The Battle for Middle-earth(tm) en klik op
     de knop 'Toevoegen/Verwijderen...' rechtsonder in beeld.
  6) Volg de instructies die in het scherm verschijnen om het spel te
     verwijderen.

==================================================================
Windows 2000
==================================================================

Wanneer je The Battle for Middle-earth(tm) probeert te spelen op een
Windows 2000-computer, moet je ervoor zorgen dat je Microsoft
Service Pack 4 hebt gedownload, evenals de nieuwste updates. 
Je kunt deze downloaden van de website van Microsoft. 


==================================================================
Problemen oplossen
==================================================================

Als de stuurprogramma's (drivers) voor je grafische kaart of geluidskaart
verouderd zijn, kan dit leiden tot een trage of haperende gameplay. In
sommige gevallen kan het spel zelfs helemaal niet worden gespeeld. Om ervoor
te zorgen dat je optimaal kunt genieten van The Battle for Middle-earth(tm),
moet je ervoor zorgen dat je de nieuwste stuurprogramma's (drivers) voor je
grafische kaart en geluidskaart hebt ge�nstalleerd. Deze stuurprogramma's
(drivers) kun je vinden op de website van de fabrikant van de hardware. Als
je niet weet welk type grafische kaart en geluidskaart je hebt, of als je
hulp nodig hebt bij het vernieuwen van de stuurprogramma's (drivers) op je
systeem, verwijzen we je naar de documentatie die hoort bij je systeem of
de randapparatuur.


==================================================================
Spelgegevens en instellingen opslaan
==================================================================
De opgeslagen bestanden en andere gebruikersinstellingen van The Battle for
Middle-earth(tm) worden opgeslagen in de map 'Mijn The Battle for
Middle-earth-bestanden'. Deze map kun je openen vanaf je harde schijf, via
de map 'Documents and Settings'. Kies de map met je Windows
gebruikersnaam en open daarna de folder 'Application Data'. Als je geen map
'Application Data' kunt vinden, doe je het volgende:

  1) Open de Windows Verkenner door de Windows-toets ingedrukt te houden
     en daarna op 'E' te drukken.
  2) Klik in de bovenste menubalk op 'Extra'.
  3) Kies in het afrolmenu 'Mapopties'.
  4) Kies het tabblad 'Weergave'.
  5) Plaats een bolletje in het cirkeltje voor 'Verborgen mappen en
     bestanden tonen' onder 'Geavanceerde instellingen'.
  6) Klik op 'Toepassen' en daarna op 'OK'.
  7) De map 'Application Data' zou nu zichtbaar moeten zijn.

Onthoud dat er maar 1 automatisch opslagbestand is voor The Battle for 
Middle-earth(tm). Als je een van de campagnes start en daarna
overschakelt naar de andere campagne, wordt het automatisch opslagbestand
van je vorige campagne overschreven.


==================================================================
Opdrachtregels
==================================================================

The Battle for Middle-earth(tm) ondersteunt verschillende opdrachtregels
waarmee je zelf kunt bepalen hoe je het spel draait. Om een opdrachtegel
in te stellen, doe je het volgende:

  1) Ga naar de locatie met het uitvoerbare bestand van het spel. Dit
     bestand vind je over het algemeen in C:\Program Files\EA Games\
     The Battle for Middle-earth(tm)\lotrbfme.exe.
  2) Klik met rechts op het icoontje van lotrbfme.exe en kies de optie
     'Snelkoppeling maken'.
  3) Op deze manier cre�er je een snelkoppeling naar het uitvoerbare
     bestand. Deze snelkoppeling wordt in dezelfde map geplaatst. De
     naam van deze snelkoppeling is meestal als volgt: 'Snelkoppeling naar
     lotrbfme.exe'.
  4) Klik met rechts op deze nieuwe snelkoppeling en kies 'Eigenschappen'.
  5) Voeg een spatie toe aan het einde van de regel bij 'Doel:', 
     gevolgd door een liggen streepje '-'. Voeg daarna de opdrachtregel
     toe.

De volgende opdrachtregels worden ondersteund in The Battle for
Middle-earth(tm):

     -noshellmap (hiermee start je het spel zonder de doorlopende 
                 Barad-d�r achtergrond)
     -mod (hiermee kunnen mod-fans eigen content in het spel laden.
          Voeg de opdrachtregel '-mod' toe aan het doel 'lotrbfme.exe',
          gevolgd door de naam van het betreffende .big-bestand of door
          de betreffende directory. De regel zou er als volgt moeten
          uitzien: 
          "C:\Program Files\EA Games\The Battle for Middle-earth(tm)\
          lotrbfme.exe" -mod MijnLOTRMod.big. Alle zelfgemaakte content
          moet geplaatst worden in de map 'Mijn The Battle for
          Middle-earth-bestanden')
     -noaudio (hiermee start je het spel zonder geluid)
     -xres (hiermee geef je een specifieke horizontale resolutie aan voor
           het spel)
     -yres (hiermee geef je een specifieke verticale resolutie aan voor
           het spel)


==================================================================
Bekende problemen
==================================================================
Bij sommige grafische kaarten die een combinatie van twee monitoren
aansturen, kan een verstoring in de beeldweergave optreden. Als dit
probleem zich voordoet, ga je naar het hoofdmenu van het spel en klik
je op Opties. Kies onder Beeldinstellingen een andere resolutie en
klik op Accepteren. Het probleem zou nu verholpen moeten zijn. Als je
wilt kun je later je oorspronkelijke resolutie weer instellen.

Als je het spel speelt met een ATI Radeon 7500 grafische kaart, is
het mogelijk dat je subtiele, gekleurde 'regenboogeffecten' ziet in
het spel. Dit zou niet al te vaak moeten voorkomen en zou de
gameplay niet in de weg moeten zitten. Als je dit probleem ondervindt,
raden we je aan te surfen naar de website van ATI om te controleren
of je gebruikmaakt van de nieuwste stuurprogramma's (drivers).

Bij gebruik van bepaalde Creative Labs Audigy geluidskaarten
is het mogelijk dat je problemen ondervindt met het geluid. Dit gebeurt
als je de EAX3-optie in het optiemenu selecteert. 
Zorg ervoor dat de stuurprogramma's (drivers) van je Audigy
up-to-date zijn.

Als je de EAX3-optie selecteert, zorg er dan voor dat je het juiste
luidsprekertype hebt geselecteerd in Windows. Om dit te controleren, doe
je het volgende:

  1) Open het menu 'Start'.
  2) Kies 'Instellingen'.
  3) Kies 'Configuratiescherm'.
  4) Kies 'Geluiden en audioapparaten'.
  5) Kies de tab 'Volume'.
  6) Klik onder 'Luidsprekerinstellingen' op 'Geavanceerd...'.
  7) Kies de tab 'Luidsprekers'.
  8) Kies in het afrolmenu voor 'Luidsprekeropstelling:' de 
     betreffende luidsprekeropstelling.
  9) Klik op 'Toepassen' en vervolgens op 'OK'.

Als je een screensaver hebt ingesteld en de screensaver actief
wordt terwijl het spel een missie laadt, wordt het spel geminimaliseerd
in je Windows taakbalk. Je vindt deze taakbalk onderin je Windows
bureaublad. Als dit gebeurt, raden we je aan de screensaver uit te
schakelen en het spel weer beeldvullend te maken door te klikken op 
de knop 'Lord of the Rings The Battle for Middle-earth' op je taakbalk.

Gebruikers van Windows 2000 kunnen mogelijk, na het afsluiten van een
vrij gevecht met zes of meer computergestuurde tegenstanders, een trage
overgang terug naar het hoofdmenu ervaren. Als een dergelijke vertraging
optreedt, kun je dit mogelijk oplossen door in het optiemenu van het
spel de instelling Details op Laag te zetten.



==================================================================
Multiplayer-informatie
==================================================================

Wanneer je probeert The Battle for Middle-earth(tm) online te spelen,
moet je erop letten dat het minimum aantal tekens voor je bijnaam
vier tekens is. Als je probeert in te loggen met een bijnaam die 
bestaat uit drie of minder tekens, kun je niet klikken op de knop
Inloggen. 
Let er ook op dat het maximum aantal tekens voor je bijnaam 15 is,
en dat je bijnaam moet beginnen met een letter uit het alfabet.

Je multiplayer-statistieken worden automatisch bijgehouden en alleen
bijgeschreven op een 1-tegen-1 of 2-tegen-2 ranglijst wanneer je een
QuickMatch speelt. Bij QuickMatch wordt een andere gebruiker binnen
een bepaald percentage van je rang en je pingwaarden gezocht om tegen te
spelen. Je kunt op de knop 'Zoeken uitbreiden' klikken om de rang- en
pingrestricties op te heffen.


==================================================================
NAT/Firewall
==================================================================

Als je The Battle for Middle-earth(tm) speelt vanachter een USRobotics 
router, moet je de firmware van het apparaat bijwerken tot de nieuwste
versie, versie 2.7 of hoger. Surf naar de supportsite van USRobotics voor
details.

Als je het spel vanachter een D-Link router speelt, klik je in het
online optiemenu van The Battle for Middle-earth(tm) op Zendvertraging.
Om daarheen te gaan vanuit het hoofdmenu, klik je op de knop Multiplayer,
daarna op de knop Online en log je in volgens de normale procedure. Klik
op Opties nadat je succesvol bent ingelogd. 
Daarnaast moet je, als je het spel vanachter een D-Link DI-704 speelt, de
firmware van het apparaat bijwerken tot versie 2.75, build 3 of hoger. Surf
naar de supportsite van D-Link voor details.

Als je het spel vanachter een D-Link DI-604 router speelt, moet je 2 regels
toevoegen aan het bestand 'options.ini'. Je vindt dit bestand in je map 
'Mijn The Battle for Middle-earth-bestanden'. Lees de paragraaf 
Spelgegevens en instellingen opslaan eerder in deze Leesmij om te weten
te komen hoe je deze map zichtbaar kunt maken. Om het bestand
'options.ini' te bewerken, dubbelklik je erop. Vervolgens voeg je de volgende
regels toe aan het einde van het bestand:

   FirewallBehavior=19
   FirewallPortAllocationDelta=2

Als je klaar bent met het toevoegen van deze regels, sluit je het
bestand 'options.ini'. Windows zal je vragen of je het bestand wilt
opslaan. Kies 'Ja'. Als je je bestand 'options.ini' hebt bijgewerkt,
moet je NAT niet verversen. Meer details over het verversen van NAT vind
je hieronder, aan het einde van deze paragraaf.

Onthoud dat je problemen kunt ondervinden met verbinden als meerdere
spelers vanachter een enkele D-Link DI-604-verbinding spelen.

Onthoud ook dat Belkin en USRobotics routers onoverkomelijke problemen
kennen als ze proberen een verbinding te maken met elkaar
via QuickMatch. Daarnaast kennen de Belkin en USRobotics routers ook
problemen met het maken van een verbinding met D-Link DI-604 routers via
Quick Match.

Als je The Battle for Middle-earth(tm) vanachter een softwarematige
firewall speelt, moet je het uitvoerbare bestand (het .exe-bestand)
van het spel toevoegen aan de lijst met 'Exceptions or Allowed Programs'
(uitzonderingen of toegestane programma's). Deze lijst wordt geleverd door
je softwarematige firewall. Je moet je softwarematige firewall vertellen wat
het pad is naar het uitvoerbare bestand van het spel. Je vindt dit bestand
over het algemeen in C:\Program Files\EA Games\The Battle for Middle-
earth(tm)\lotrbfme.exe. Je moet ook het game.dat-bestand toevoegen aan de lijst,
dat vind je in dezelfde directory als hierboven wordt genoemd.*

Het zou niet nodig moeten zijn om 'Port Forwarding/Port Triggering'
te gebruiken wanneer je het spel speelt vanachter je firewall. The Battle for
Middle-earth(tm) zou goed moeten werken vanachter de meeste firewalls.

Als je je routerinstellingen hebt aangepast sinds de laatste keer dat
je The Battle for Middle-earth(tm) hebt gespeeld, moet je klikken op de knop
NAT verversen in het online optiemenu van het spel. Om daarheen te gaan
vanuit het hoofdmenu, klik je op de knop Multiplayer, daarna op de knop
Online en log je in volgens de normale procedure. Klik op Opties nadat
je succesvol bent ingelogd.


The Battle for Middle-earth(tm) maakt gebruik van UDP-poorten 8088-28088.

Als je algemene problemen ondervindt met NAT-verbindingen, sluit je
het spel af, verwijder je het bestand 'options.ini' uit je map 'Mijn
The Battle for Middle-earth-bestanden' en klik je op de knop 
NAT verversen in het online optiemenu van het spel. 

Daarnaast moet je, als je gebruikmaakt van een combinatie van een kabelmodem
en een router, contact opnemen met je internet serviceprovider/
kabelleverancier om de interne firewall van je modem uit te schakelen.


==================================================================
*Administrator-rechten
==================================================================

Onthoud dat het waarschijnlijk niet mogelijk is om The Battle for Middle-
earth(tm) toe te voegen aan de uitzonderingenlijst van je softwarematige
firewall als je niet bent aangemeld in Windows met administrator-rechten.
Meld je aan vanaf een account met administrator-rechten of zet je huidige
account om in een account met administrator-rechten. 
Om dit te doen, volg je de volgende stappen:

  1) Meld je af in Windows en meld je weer aan via een administrator-
     account.
  2) Als je bent aangemeld, klik je op 'Start' in de Windows taakbalk.
  3) Kies 'Instellingen'.
  4) Kies 'Configuratiescherm'.
  5) Kies 'Gebruikersaccounts'.
  6) Klik op 'Een account wijzigen'.
  7) Klik op de naam van je account.
  8) Klik op 'Het type account wijzigen'.
  9) Selecteer 'Computerbeheerder'.
  10) Klik op 'Het type van een account wijzigen'.
  11) Je hebt nu administrator-rechten toegewezen aan je account.
        


==================================================================
Credits
==================================================================

The Lord of the Rings(tm), The Battle for Middle-earth(tm) maakt 
Gebruik van MP3-decoderingstechnologie. Fraunhofer en Thomson zijn
de ontwikkelaars/eigenaren van dit MP3-formaat.

