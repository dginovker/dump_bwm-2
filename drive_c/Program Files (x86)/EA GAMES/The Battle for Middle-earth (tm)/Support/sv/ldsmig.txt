==================================================================
Juridisk information
==================================================================

DENNA PROGRAMVARA TILLHANDAH�LLES I BEFINTLIGT SKICK UTAN N�GRA 
GARANTIER, VARKEN UTTALADE ELLER ANTYDDA, INKLUSIVE GARANTIER 
ANG�ENDE S�LJBARHET OCH L�MPLIGHET F�R ETT SPECIFIKT �NDAM�L 
VILKA H�RMED F�RNEKAS. ELECTRONIC ARTS KAN INTE I N�GOT FALL 
H�LLAS ANSVARIGT F�R SPECIFIKA SKADOR, TILLF�LLIGA SKADOR ELLER 
F�LJDSKADOR SOM UPPKOMMIT SOM RESULTAT AV INNEHAV, ANV�NDNING 
ELLER TEKNISKT FEL HOS DENNA PROGRAMVARA FR�N ELECTRONIC ARTS.

VISSA L�NDER TILL�TER INTE BEGR�NSNINGAR AV GARANTIN OCH/ELLER 
GARANTITIDEN, S� UNDANTAGEN FR�N TILLVERKARANSVAR KANSKE INTE
G�LLER DIG. DENNA GARANTI GER DIG SPECIFIKA R�TTIGHETER. DU KAN 
�VEN HA ANDRA R�TTIGHETER VILKA VARIERAR FR�N LAND TILL LAND.

ELECTRONIC ARTS F�RBEH�LLER SIG R�TTEN ATT G�RA F�RB�TTRINGAR 
I DENNA FILOCH I PROGRAMVARAN UTAN F�RVARNING. DENNA FIL OCH 
DEN MEDF�LJANDE PROGRAMVARAN �R UPPHOVSR�TTSSKYDDAD. MED ENSAMR�TT.
INGA DELAR AV DENNA FIL ELLER PROGRAMVARAN F�R KOPIERAS,
REPRODUCERAS, �VERS�TTAS ELLER REDUCERAS TILL N�GOT ELEKTRONISKT
MEDIUM ELLER TILL MASKINL�SBAR FORM UTAN SKRIFTLIGT MEDGIVANDE
FR�N ELECTRONIC ARTS.


==================================================================
Electronic Arts Copyright
==================================================================

Programvara och dokumentation � 2004 Electronic Arts Inc.

MED ENSAMR�TT.


==================================================================
Slaget om Midg�rd, H�rskarringen(tm) 
Version 1.0 L�smig-fil

11 november 2004
==================================================================

Tack f�r att du k�pt Slaget om Midg�rd. Denna l�smig-fil 
inneh�ller information som kommer att hj�lpa dig p�b�rja din f�rd
genom Midg�rd.


==================================================================
Systemkrav
==================================================================

Slaget om Midg�rd kr�ver DirectX 9.0b.
F�r att installera DirectX 9.0b eller senare, bes�k:

  http://www.microsoft.com/windows/directx/default.aspx

REKOMMENDERAT:

  - Windows XP eller Windows 2000
  - 1.3 GHz Intel Pentium IV eller AMD Athlon processor
  - 256 MB RAM
  - Nvidia GeForce 2 eller motsvarande grafikkort med 32 MB RAM
  - DirectX 9.0b-kompatibelt ljudkort
  - 4 GB ledigt utrymme p� h�rddisken

Observera att �ven om du kan spela Slaget om Midg�rd med en dator 
som uppfyller ovanst�ende krav, kan spelupplevelsen bli
angen�mare med mer RAM ut�ver de 256 MB som kr�vs. 

Nvidia GeForce 2 MX st�ds ej.

==================================================================
Installationsanvisningar
==================================================================

Innan du k�r Slaget om Midg�rd, se till att du har de senaste
drivrutinerna till ditt grafik- och ljudkort installerade. Du kan
ladda ner l�mpliga drivrutiner fr�n korttillverkarnas hemsidor.

Undvik att k�ra andra program i bakgrunden, d� detta kan skapa
konflikt med Slaget om Midg�rd. Detta inkluderar
virusskyddsprogram som kan stj�la prestanda.

F�r att installera Slaget om Midg�rd:

  1) Stoppa i installationsskivan till Slaget om Midg�rd
     i din DVD-ROM-enhet.

     Om skivan inte �ppnas automatiskt kanske denna funktion 
     �r avst�ngd p� din dator. F�r att aktivera "Spela upp automatiskt", 
     l�s i din instruktionsbok till Windows.

     Om du inte kan eller vill aktivera "Spela upp automatiskt", var god
     bl�ddra fram Slaget om Midg�rds DVD-ROM med 
     Windows Explorer, och dubbelklicka p� programmet "Setup"
     i rotkatalogen.

  2) F�lj de instruktioner som visas f�r att installera
     Slaget om Midg�rd.

  3) Observera att du m�ste registrera Slaget om Midg�rd under
     installationsprocessen f�r att kunna spela spelet online.
     Du kan registrera spelet senare genom att klicka p� knappen
     "Registrera" i spelets Online-meny.

Om du har problem med din CD-nyckel till Slaget om Midg�rd,
eller beh�ver annan teknisk support, var god klicka p� knappen 
"Teknisk support" i spelets AutoPlaymeny. Du kan �ven bes�ka 
den officiella produktwebbsajten nedan och v�lja alternativet
"Support". Du m�ste registrera din produkt f�r att f� full
kundsupport.

http://www.eagames.com/official/lordoftherings/thebattleformiddleearth/us/


==================================================================
Avinstallera Slaget om Midg�rd
==================================================================

  F�r att avinstallera Slaget om Midg�rd:

  1) �ppna Startmenyn.
  2) V�lj "Inst�llningar".
  3) V�lj "Kontrollpanelen".
  4) V�lj "L�gg till/Ta bort program".
  5) Markera "Slaget om Midg�rd" och klicka p�
     kanppen "Ta bort" under spelets namn.
  6) F�lj de instruktioner som visas f�r att avinstallera.




==================================================================
Windows 2000
==================================================================

Om du f�rs�ker k�ra Slaget om Midg�rd p� en dator med Windows 2000,
m�ste du se till att ladda ner Service Pack 4 och alla de senaste
uppdateringarna.
Du kan ladda ner dessa p� Microsofts webbsajt.


==================================================================
Fels�kning
==================================================================

En inaktuell grafik- eller ljuddrivrutin kan leda till l�ngsamt
och ryckigt spel, eller i vissa fall helt hindra spelet att fungera.
F�r att s�kerst�lla en njutbar upplevelse med Slaget om Midg�rd,
se till att du har de senaste drivrutinerna f�r ljud och grafik 
installerade. Dessa finns normalt att lada ner p� dator- eller 
korttillverkarens hemsida. Om du �r os�ker p� vilken typ av 
grafik- eller ljudkort du har, eller beh�ver hj�lp med att 
uppdatera din dators drivrutiner, l�s dokumentationen som 
f�ljde med datorn eller kortet.


==================================================================
Sparade spel och inst�llningar
==================================================================

Sparade spel och andra anv�ndarinst�llningar till Slaget om Midg�rd
sparas i mappen "Mina Slaget om Midg�rd-filer" som oftast kan n�s
genom att �ppna mappen "Documents and Settings" p� enhet C:\,
�ppna mappen med ditt anv�ndarnamn i Windows och d�refter �ppna 
mappen "Application data". Om du inte hittar n�gon s�dan mapp, g�r 
f�ljande:

  1) �ppna ett Windows Explorer-f�nster genom att h�lla ner 
     Widowstangenten och trycka p� "E".
  2) Klicka p� "Verktyg" i toppmenyn.
  3) V�lj "Mappalternativ" i rullgardinsmenyn.
  4) V�lj fliken "Visa".
  5) Klicka p� knappen "Visa dolda filer och mappar" under
     "Avancerade inst�llningar".
  6) Klicka p� "Verkst�ll" och sedan p� "OK".
  7) Mappen "Application data" borde nu vara synlig.

Observera att det bara finns en Autosparfil till Slaget om Midg�rd.
Om du inleder ett f�ltt�g f�r ena sidan och sedan v�xlar till
den andra sidans f�ltt�g, kommer din Autosparfil att skrivas �ver.


==================================================================
Kommandoradsparametrar
==================================================================

Slaget om Midg�rd st�der ett flertal kommadoradsparametrar
som ger dig st�rre flexibilitet i hur du k�r spelet.
F�r att st�lla in en kommandoradsparameter, g�r s� h�r:

  1) Leta r�tt p� spelets exekverbara fil. Vanligen finns den 
     h�r: C:\Program\EA Games\Slaget om Midg�rd\lotrbfme.exe.
  2) H�gerklicka p� ikonen lotrbfme.exe och v�lj "Skapa genv�g".
  3) Detta skapar en genv�g till den exekverbara filen i samma 
     mapp. Vanligen f�r den namnet "Genv�g till lotrbfme.exe".
  4) H�gerklicka p� denna genv�g och v�lj "Egenskaper".
  5) L�gg till ett mellanslag i slutet av textstr�ngen i f�ltet  
     "M�l:", f�ljt av "-" samt kommandoradsparametern.

F�ljande kommadoradsparametrar st�ds av Slaget om Midg�rd:

     -noshellmap (Startar spelet utan filmslingan av Barad-d�r i 
                  i bakgrunden)
     -mod (L�ter moddare ladda in skr�ddarsydda resurser i spelet. 
           L�gg bara till kommandot "-mod" efter s�kv�gen lotrbfme.exe 
           f�ljt av namnet p� spelarskapade .big-filer eller mappar. 
           Hela s�kv�gen kommer att se ut s� h�r: 
           "C:\Program\EA Games\Slaget om Midg�rd\
           lotrbfme.exe" -mod MyLOTRMod.big. 
	   Allt spelarskapat material
           m�ste l�ggar i mappen "Mina Slaget om Mig�rd-filer")
     -noaudio (Startar spelet utan ljud)
     -xres (Tvingar spelet att anv�nda en viss horisontell uppl�sning)
     -yres (Tvingar spelet att anv�nda en viss vertikal uppl�sning))


==================================================================
K�nda problem
==================================================================

P� vissa grafikkort som driver tv� bildsk�rmar kan du uppleva
bildf�rvr�ngningar p� sk�rmen. Om du st�ter p� detta problem,
g� till spelets huvudmeny och klicka p� "Alternativ". V�lj en
annan sk�rmuppl�sning under "Grafikalternativ" och klicka p�
"Godk�nn �ndringar". Detta b�r l�sa problemet. Om du vill, kan du
v�xla tillbaka till din tidigare sk�rmuppl�sning.

Om du spelar spelet med ett ATI Radeon 7500 grafikkort, kan du 
st�ta p� svaga flerf�rgade regnb�gseffekter i spelet. Detta sker
s�llan och b�r inte p�verka spelbarheten p� minsta s�tt. Om du
st�ter p� detta problem, bes�k ATI:s webbsajt f�r att f�rs�kra
dig om att du anv�nder de senaste drivrutinerna.

Vissa Creative Labs Audigy-ljudkort kan ge problem om du valt
alternativet EAX3 i Alternativmenyn.
Se till att skaffa de senaste drivrutinerna till ditt
Audigy-ljudkort.

Om du v�ljer att aktivera EAX3-alternativet, se till att du
har r�tt h�gtalartyp vald i Windows. G�r f�ljande:

  1) I Windows, klicka p� "Start".
  2) V�lj "Inst�llningar".
  3) V�lj "Kontrollpanelen".
  4) V�lj "Ljud, tal och ljudenheter".
  5) V�lj "Justera datorns volym".
  6) Under "H�gtalarinst�llningar", klicka p� "Avancerat...".
  7) V�lj "H�gtalare".
  8) V�lj det h�gtalarsystem du anv�nder i rullgardinsmenyn.
  9) Klicka p� "Verkst�ll" och sedan p� "OK".

Om du aktiverat din sk�rmsl�ckare och den startar samtidigt som 
spelet laddar in ett uppdrag, minimeras spelet till Windows
aktivitetsf�lt (som oftast finns i Windowsskrivbordets nedre
kant). Om detta sker, st�ng av sk�rmsl�ckaren och maximera
spelet genom att klicka p� knappen "Slaget om Midg�rd, H�rskarringen"
i aktivitetsf�ltet.

Anv�ndare som k�r Windows 2000 kan uppleva en l�ngsam �verg�ng till
huvudmenyn n�r de g�r ur ett Sk�rmytslingsspel med sex eller fler
AI-styrda motst�ndare. Om du upplever en s�dan f�rd�jning, kan du
f�rs�ka r�da bot p� den genom att v�lja L�g detaljniv� i spelets 
Alternativmeny.


==================================================================
Multiplayer-information
==================================================================

N�r du f�rs�ker spela Slaget om Midg�rd online, observera att
ditt "Smeknamn" m�ste best� av minst fyra tecken. Om du f�rs�ker
logga in med ett smeknamn med tre eller f�rre tecken f�rblir
knappen "Logga in" inaktiv. Observera �ven att smeknamnet max f�r
vara 15 tecken l�ngt och m�ste b�rja med en bokstav.

Din multiplayerstatistik loggas och skickas till en stege
(1 mot 1 eller 2 mot 2) endast n�r du spelar Snabbmatch.
Snabbmatch letar upp en motst�ndare inom ett intervall i n�rheten
av din grad och ping. Du kan klicka p� knappen "Bredda s�kningen"
f�r att ta bort begr�nsningarna vad g�ller grad och ping.


==================================================================
NAT/Brandv�gg
==================================================================

Om du spelar Slaget om Midg�rd bakom en router fr�n USRobotics,
m�ste du uppgradera dess fasta program till senaste versionen,
v2.7 eller senare. Bes�k USRobotics supportsajt f�r information
om hur du ska g�ra.

Om du spelar spelat bakom en router fr�n D-Link, m�ste du bocka f�r
rutan "S�ndningsf�rdr�jning" i spelets Online-alternativmeny.
Klicka p� knappen Multiplayer i huvudmenyn, sedan p� knappen
Online, logga in som vanligt och klicka p� Alternativ n�r du
lyckats logga in. Om du spelar bakom en D-Link DI-704, m�ste du 
dessutom uppgradera dess fasta program till version 2.75 build 3
eller senare. Bes�k D-Link:s supportsajt f�r information
om hur du ska g�ra.

Om du spelar spelet bakom en D-Link DI-604 m�ste du l�gga till
tv� rader i filen options.ini. Den finns i mappen 
"Mina Slaget om Midg�rd-filer". F�r att hitta mappen, l�s i stycket
Sparade spel och inst�llningar ovan. F�r att redigera filen
options.ini, h�gerklicka p� den och v�lj "Redigera".
L�gg till f�ljande rader l�ngst ner i filen:

   FirewallBehavior = 19
   FirewallPortAllocationDelta = 2

N�r du �r klar, st�ng filen options.ini. Windows fr�gar
om du vill spara eller inte--v�lj "Ja".
Om du uppdaterar din options.ini-fil b�r du inte uppdatera NAT.
Mer information om uppdatera NAT finns i stycket om
NAT/brandv�gg i denna l�smig-fil.

Observera att om ni �r flera spelare bakom en enda D-Link 
DI-604-anslutning s� kan ni uppleva anslutningsproblem.

Det b�r ocks� noteras att Belkin- och USRoboticsroutrar �r
inkompatibla, men bara n�r de f�rs�ker ansluta till varandra
via Snabbmatch. Dessutom har routrar fr�n Belkin och USRobotics 
problem med att ansluta till D-Link DI-604-routrar 
via Snabbmatch.

Om du spelar Slaget om Midg�rd bakom ett brandv�ggprogram,
m�ste du l�gga till spelets programfil brandv�ggsprogrammets lista
"Undantag eller Till�tna program". Du m�ste ange s�kv�gen till
spelets programfil. Vanligen finns denna fil i "C:\Program\
EA Games\Slaget om Midg�rd\lotrbfme.exe". Du m�ste �ven l�gga
till filen "game.dat" (i samma katalog) i undantagslistan*.

Du b�r inte beh�va anv�nda Port-vidarebefordring/Port-triggning
f�r att spela spelet bakom din brandv�gg. Slaget om Midg�rd
b�r fungera bakom de flesta personliga brandv�ggar.

Om du har �ndrat i dina router-inst�llningar sedan sist du spelade
Slaget om Midg�rd, m�ste du klicka p� knappen "Uppdatera NAT" i 
spelets meny f�r Online-alternativ. Klicka p� knappen Multiplayer
i huvudmenyn, sedan p� knappen Online, logga in som vanligt 
och klicka p� Alternativ n�r du lyckats logga in.

Slaget om Midg�rd anv�nder UDP-portarna 8088-28088.

Om du upplever anslutningsproblem med NAT, avsluta spelet,
radera filen options.ini i mappen "Mina Slaget om Midg�rd-filer",
starta om spelet och klicka p� knappen "Uppdatera NAT" i menyn
Onlinealternativ.

Om du dessutom anv�nder en kombination av ett kabelmodem och
en router, b�r du kontakta din Internet/kabeloperat�r f�r
att deaktivera ditt modems inbyggda brandv�gg.


==================================================================
*Administrat�rsprivilegier
==================================================================

Observera att du kanske inte kan l�gga till Slaget om Midg�rd
i brandv�ggprogrammets undantagslista om du inte �r inloggad
som administrat�r i Windows. Antingen f�r du spela fr�n ett
administrat�rskonto, eller l�gga till ditt konto i administrat�rs-
listan. Det g�r till s� h�r:

  1) Logga ut fr�n Windows och logga in med ett Administrat�rskonto.
  1) N�r du loggat in, klicka p� "Start" i Windows aktivitetsf�lt.
  2) V�lj "Inst�llningar".
  3) V�lj "Kontrollpanelen".
  4) V�lj "Anv�ndarkonton".
  5) V�lj "�ndra ett konto".
  6) Klicka p� ditt kontonamn.
  7) Klicka p� "�ndra min kontotyp".
  8) V�lj "Datoradministrat�rer".
  9) Klicka p� "Byt kontotyp".


==================================================================
Medverkande
==================================================================

Slaget om Midg�rd, H�rskarringen(tm) anv�nder sig av 
MP3-avkodningsteknik. Fraunhofer & Thomson �r utvecklare / �gare 
av detta MP3-format.




