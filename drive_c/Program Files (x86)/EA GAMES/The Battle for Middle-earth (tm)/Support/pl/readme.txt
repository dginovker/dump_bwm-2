==================================================================
Informacje prawne
==================================================================

TO OPROGRAMOWANIE JEST DOSTARCZONE "JAK JEST", BEZ �ADNYCH 
GWARANCJI, PISEMNYCH ANI DOMY�LNYCH, ��CZNIE Z GWARANCJ� SPRZEDA�Y 
I DOSTOSOWANIA DO OKRE�LONYCH POTRZEB. FIRMA ELECTRONIC ARTS W 
�ADNYM WYPADKU NIE PONOSI ODPOWIEDZIALNO�CI ZA JAKIEKOLWIEK CELOWE 
LUB PRZYPADKOWE SZKODY WYNIKAJ�CE Z POSIADANIA, U�YTKOWANIA, LUB 
B��DNEGO DZIA�ANIA TEGO OPROGRAMOWANIA FIRMY ELECTRONIC ARTS.

NIEKT�RE PA�STWA NIE POZWALAJ� NA OGRANICZENIE CZASU DOMY�LNEJ 
GWARANCJI I/LUB WYJ�TK�W LUB OGRANICZE� GWARANCJI Z POWODU 
PRZYPADKOWYCH SZK�D, DLATEGO POWY�SZE OGRANICZENIE GWARANCJI MO�E 
NIE MIE� ZASTOSOWANIA. NINIEJSZA GWARANCJA NADAJE KLIENTOWI 
OKRE�LONE PRAWA, KT�RE MOG� ZOSTA� POSZERZONE PRZEZ PRAWO PA�STWA, 
W KT�RYM NABYTO OPROGRAMOWANIE.

FIMRA ELECTRONIC ARTS ZASTRZEGA SOBIE PRAWO DO ULEPSZEANIA TEGO 
PLIKU I OPROGRAMOWANIA W DOWOLNEJ CHWILI, BEZ WCZE�NIEJSZEGO 
UPRZEDZENIA. PLIK I OPROGRAMOWANIE S� CHRONIONE PRAWEM AUTORSKIM. 
WSZYSTKIE PRAWA ZASTRZE�ONE. TEGO PLIKU ANI OPROGRAMOWANIA NIE 
WOLNO KOPIOWA�, PRODUKOWA�, T�UMACZY� NA INNY J�ZYK ANI 
MODYFIKOWA�, W CA�O�CI ANI W CZʌCI, BEZ UPRZEDNIEJ, PISEMNEJ ZGODY 
FIRMY ELECTRONIC ARTS.



==================================================================
Prawa autorskie Electronic Arts
==================================================================

Oprogramowanie i dokumentacja: (c) 2004 Electronic Arts Inc.

WSZELKIE PRAWA ZASTRZE�ONE.


==================================================================
"W�adca pier�cieni" (tm), "Bitwa o �r�dziemie" (tm) 
Plik Readme, wersja 1.0

11 listopad 2004
==================================================================

Dzi�kujemy za zakup gry "Bitwa o �r�dziemie" (tm). W tym pliku 
Readme znajduj� si� informacje, kt�re u�atwi� Ci podr� przez 
krainy �r�dziemia.


==================================================================
Wymagania systemowe
==================================================================

Gra "Bitwa o �r�dziemie" (tm) wymaga DirectX 9.0b. Aby zainstalowa� 
DirectX w wersji 9.0b lub nowszej, zajrzyj na stron�:

  http://www.microsoft.com/windows/directx/default.aspx

MINIMALNE WYMAGANIA SYSTEMOWE:

  - Windows XP lub Windows 2000
  - Procesor 1.3 GHz Intel Pentium IV lub AMD Athlon
  - 256 MB RAM
  - Karta graficzna Nvidia GeForce 2 z 32 MB RAM lub jej 
    odpowiednik
  - Karta d�wi�kowa, kompatybilna z DirectX 9.0b
  - 4 GB wolnego miejsca na twardym dysku

Karta Nvidia GeForce 2 MX nie jest os�ugiwana.

Prosimy pami�ta�, �e cho� gra w Bitw� o �r�dziemie(tm) na komputerze
spe�niaj�cym powy�sze wymagania b�dzie mo�liwa, to jako�� rozgrywki
poprawi si� w przypadku posiadania wi�cej ni� wymaganych 256 MB 
pami�ci RAM.

==================================================================
Opis instalacji
==================================================================

Przed uruchomieniem gry "Bitwa o �r�dziemie" (tm), upewnij si�, czy 
w systemie zainstalowano najnowsze sterowniki do karty graficznej  
i d�wi�kowej. Odpowiednie sterowniki mo�na pobra� ze strony 
internetowej producenta sprz�tu.

Nie nale�y uruchamia� aplikacji dzia�aj�cych w tle, gdy� mo�e to 
spowodowa� konflikty z gr�. Dotyczy to mi�dzy innymi program�w 
antywirusowych, kt�re mog� znacz�co obni�y� wydajno��.

Aby zainstalowa� gr� "Bitwa o �r�dziemie" (tm):

  1) Umie�� p�yt� instalacyjn� z gr� "Bitwa o �r�dziemie" (tm)  
     w  nap�dzie DVD-ROM.

     Je�li p�yta nie uruchomi si� automatycznie, mo�e to oznacza� 
     �e w systemie wy��czono funkcj� autoodtwarzania. Zajrzyj do 
     swojego podr�cznika u�ytkowania systemu Windows, aby 
     dowiedzie� si�, jak w��czy� autoodtwarzanie.

     Je�li nie jeste� w stanie w��czy� autoodtwarzania, lub nie 
     chcesz tego robi�, otw�rz p�yt� z gr� "Bitwa o �r�dziemie"(tm) 
     za pomoc� eksploratora Windows i kliknij dwa razy na "Setup".

  2) Aby zainstalowa� gr�, post�puj zgodnie z poleceniami   
     pojawiaj�cymi si� na ekranie.

  3) Aby gra� w "Bitw� o �r�dziemie" (tm) przez internet, musisz 
     zarejestrowa� gr� podczas instalacji lub p�niej, klikaj�c na 
     przycisku "Rejestracja" w menu sieciowym.

Je�li wyst�pi� problemy z dostarczonym wraz z gr� kluczem produktu, 
lub potrzebujesz pomocy technicznej, kliknij na przycisku "Pomoc
Techniczna" w menu startowym gry. Mo�esz r�wnie� odwiedzi� oficjaln�
stron� internetow� gry i  wybra� opcj� "Support". Aby w pe�ni 
skorzysta� z pomocy dzia�u obs�ugi klienta, konieczne jest 
zarejestrowanie gry.

http://www.eagames.com/official/lordoftherings/thebattleformiddleearth/us/


==================================================================
Usuwanie gry "Bitwa o �r�dziemie" (tm)
==================================================================

  Aby usun�� gr� "Bitwa o �r�dziemie" (tm):

  1) Wejd� do menu Start systemu Windows.
  2) Wybierz "Ustawienia".
  3) Wybierz "Panel sterowania".
  4) Wybierz "Dodaj/usu� programy".
  5) Zaznacz "Bitwa o �r�dziemie" (tm)" i kliknij na przycisku 
     "Zmie�/usu�" pod nazw� gry.
  6) Post�puj zgodnie z instrukcjami pojawiaj�cymi si� na ekranie.


==================================================================
Windows 2000
==================================================================

Aby uruchomi� gr� Bitwa o �r�dziemie (tm) w �rodowisku 
Windows 2000, musisz zainstalowa� Microsoft Service Pack 4 oraz 
najnowsze aktualizacje. 
Te elementy mo�esz �ci�gn�� ze strony sieciowej firmy Microsoft. 


==================================================================
Rozwi�zywanie problem�w
==================================================================

Przestarza�e sterowniki do karty graficznej lub d�wi�kowej mog� 
spowodowa� powolne lub skokowe dzia�anie gry. W niekt�rych 
sytuacjach mo�e to ca�kowicie uniemo�liwi� uruchomienie gry. Aby 
gra dzia�a�a z pe�n� wydajno�ci�, nale�y zainstalowa� najnowsze 
wersje sterownik�w do karty graficznej i d�wi�kowej. Sterowniki 
takie zazwyczaj pobiera si� ze strony internetowej producenta 
sprz�tu. Je�li nie wiesz, jakiego typu karta graficzna lub d�wi�kowa 
znajduje si� w Twoim komputerze, albo potrzebujesz pomocy w 
zakresie aktualizacji sterownik�w, zajrzyj do instrukcji systemu 
operacyjnego lub odpowiedniego urz�dzenia.

==================================================================
Zapisane gry i ustawienia
==================================================================

Pliki zapisanych rozgrywek z gry "Bitwa o �r�dziemie" (tm) i 
ustawienia u�ytkownika s� przechowywane w folderze "My Battle for 
Middle-earth Files", kt�ry zazwyczaj znajduje si� na partycji C,  
w folderze "Dokumenty i ustawienia", dalej w folderze oznaczonym 
nazw� u�ytkownika i w folderze "Dane aplikacji". Je�li w podanym 
miejscu nie wida� folderu "Dane aplikacji", wykonaj nast�puj�ce 
czynno�ci:

  1) Wywo�aj eksploratora Windows przytrzymuj�c klawisz Windows  
     i naciskaj�c klawisz E.
  2) Kliknij na "Narz�dzia" na g�rnym pasku.
  3) Z rozwijanego menu wybierz "Opcje folder�w".
  4) Wybierz zak�adk� "Widok".
  5) Kliknij na przycisku "Poka� ukryte pliki i foldery"  
     w "Ustawieniach zaawansowanych ".
  6) Kliknij na "Zastosuj", a nast�pnie "OK".
  7) Folder "Dane aplikacji" powinien by� teraz widoczny.

Prosimy pami�ta�, �e w grze Bitwa o �r�dziemie(tm) istnieje tylko 
jeden plik AUTOZAPISU. Je�li rozpoczniesz jedn� z kampanii, 
a p�niej prze��czysz na drug�, tw�j plik AUTOZAPISU z poprzedniej
kampanii zostanie zast�piony nowym.


==================================================================
Parametry linii polece�
==================================================================

Gra Bitwa o �r�dziemie(tm) pozwala na wykorzystanie kilku parametr�w
linii polece�, kt�re pozwol� Ci dostosowa� spos�b uruchamiania gry
do w�asnych potrzeb. Aby wprowadzi� parametr linii polece�, musisz 
wykona� nast�puj�ce czynno�ci:

  1) Przejd� do miejsca, w kt�rym znajduje si� plik wykonywalny gry.
     Zwykle tym miejscem jest:
     C:\Program Files\EA Games\Bitwa o �r�dziemie(tm)\ 
     lotrbfme.exe.
  2) Kliknij prawym przyciskiem na ikonie lotrbfme.exe i wybierz 
     opcj� "Utw�rz skr�t".
  3) Spowoduje to stworzenie skr�tu do pliku wykonywalnego 
     i umieszczenie go w tym samym folderze. Domy�lnie b�dzie nosi� 
     nazw� "Skr�t do lotrbfme.exe".
  4) Kliknij prawym przyciskiem na nowo stworzonym skr�cie i wybierz
     "W�a�ciwo�ci".
  5) Dodaj spacj�, a nast�pnie my�lnik "-", na ko�cu wyra�enia w polu 
     "Element docelowy:", po czym wpisz parametr linii polece�.

Gra Bitwa o �r�dziemie(tm) pozwala na wykorzystanie nast�puj�cych 
parametr�w:

     -noshellmap (Pozwala na uruchomienie gry bez Barad-duru w tle)
     -mod (Pozwala tw�rcom modyfikacji na wczytywanie w�asnych 
           element�w do gry. Po prostu dodaj parametr "-mod" do
           po �cie�ce docelowej pliku lotrbfme.exe, a nast�pnie
           wpisz nazw� dowolnego pliku typu .big stworzonego przez 
           u�ytkownika, lub nazw� folderu u�ytkownika. Prawid�owo
           podana �cie�ka mo�e przyk�adowo wygl�da� tak:: 
           "C:\Program Files\EA Games\Bitwa o �r�dziemie(tm)\
           lotrbfme.exe" -mod MojMODLOTR.big. Wszystkie elementy 
           stworzone przez u�ytkownik�w, musza by� umieszczone 
           w folderze "My Battle for Middle-earth Files")
     -noaudio (Uruchamia gr� bez d�wi�ku)
     -xres (Wymusza u�ycie w grze okre�lonej rozdzielczo�ci poziomej)
     -yres (Wymusza u�ycie w grze okre�lonej rozdzielczo�ci pionowej)



==================================================================
Znane problemy
==================================================================

Na niekt�rych kartach graficznych, kt�re obs�uguj� wy�wietlanie w 
trybie dwumonitorowym, mog� wyst�pi� zniekszta�cenia obrazu. Je�li
spotkasz si� z takim problemem, wejd� do g��wnego menu gry i 
kliknij na "Opcje". Wybierz inn� rozdzielczo�� w "Opcjach 
wy�wietlania", a nast�pnie kliknij na "Akceptuj zmiany". Powinno to
naprawi� problem. Je�li chcesz, mo�esz p�niej wr�ci� do pierwotnej
rozdzielczo�ci.

Je�li grasz na komputerze wyposa�onym w kart� graficzn� ATI Radeon 
7500, mo�esz zauwa�y� delikatny efekt wielobarwnej t�czy. Efekt ten
nie powinien by� cz�sto widoczny i w �aden spos�b nie przeszkadza
w grze. Je�li napotkasz na taki problem, udaj si� na witryn�
sieciow� ATI, aby sprawdzi�, czy masz najnowsze sterowniki.

W przypadku niekt�rych kart d�wi�kowych Creative Labs Audigy, mog� 
pojawi� si� problemy z odtwarzaniem, je�li w menu opcji zosta�a 
wybrana pozycja EAX3. Sprawd�, czy posiadasz najnowsze sterowniki 
do swojej karty Audigy.

Je�li wybierzesz opcj� EAX3, upewnij si�, �e w Windows zaznaczono
odpowiedni rodzaj g�o�nik�w. Mo�na tego dokona� w nast�puj�cy 
spos�b:

  1) W Windows kliknij na "Start".
  2) Wybierz "Ustawienia".
  3) Wybierz "Panel sterowania".
  4) Wybierz "D�wi�ki i ustawienia audio".
  5) Wybierz zak�adk� "G�o�no��".
  6) W "Ustawieniach g�o�nik�w" kliknij na "Zaawansowane...".
  7) Wybierz "G�o�niki".
  8) Wybierz odpowiedni� opcj� z rozwijanego menu "Ustawienia 
     g�o�nik�w:".
  9) Kliknij na "Zastosuj", a nast�pnie na "OK".

Je�li masz w��czony wygaszacz ekranu i uruchomi si� on w czasie 
wczytywania misji, gra zostanie zminimalizowana do paska zada�
Windows, kt�ry zwykle umieszczony jest na dole pulpitu. Je�li si�
tak stanie, wy��cz wygaszacz ekranu i wr�� do gry klikaj�c na 
przycisku "W�adca Pier�cieni: Bitwa o �r�dziemie", na pasku zada�.

U�ytkownicy korzystaj�cy z systemu Windows 2000 mog� do�wiadczy�
op�nienia przy wchodzeniu do g��wnego menu po zako�czeniu potyczki
z sze�cioma lub wi�cej przeciwnikami sterowanymi przez komputer.
Je�li wyst�puj� u Ciebie takie problemy, mo�esz spr�bowa� je
rozwi�za� ustawiaj�c niski poziom szczeg��w w menu opcji gry.  


==================================================================
Informacje dotycz�ce rozgrywki dla wielu graczy
==================================================================

Je�li chcesz gra� w Bitw� o �r�dziemie(tm) w sieci, pami�taj �e
minimalna liczba znak�w dla pseudonimu to cztery. Je�li pr�bujesz
zalogowa� si� u�ywaj�c pseudonimu posiadaj�cego trzy znaki lub 
mniej, przycisk "Login" pozostanie nieaktywny. Pami�taj tak�e, �e
najwi�ksza dopuszczalna d�ugo�� pseudonimu to 15 znak�w, a na 
pocz�tku pseudonimu nie mog� znajdowa� si� znaki spoza alfabetu.

Statystki rozgrywek sieciowych b�d� automatycznie zapisywane i 
u�ywane w rankingach 1na1 i 2na2, jedynie je�li grasz w "Szybki 
mecz". Rozgrywka tego typu pozwala na znalezienie u�ytkownika o
odpowiednim rankingu i rodzaju po��czenia. Mo�esz klikn�� na 
przycisku "Szersze wyszukiwanie", aby usun�� ograniczenia rankingu
i jako�ci po��czenia.

==================================================================
NAT/Zapora po��cze�
==================================================================

Je�li grasz w Bitw� o �r�dziemie(tm) i korzystasz z routera firmy
USRobotics, konieczne b�dzie zaktualizowanie jego oprogramowania
(firmware) do wersji 2.7 lub nowszej. Wi�cej informacji na temat,
jak tego dokona�, znajdziesz na stronie pomocy firmy USRobotics.

Je�li grasz korzystaj�c z routera firmy D-Link, musisz zaznaczy� 
pozycj� "Op�nienie wysy�ania" w menu opcji sieciowych gry Bitwa
o �r�dziemie(tm). Aby wej�� tam z g��wnego menu, kliknij na 
przycisku "Gra wieloosobowa", nast�pnie na przycisku "Sie�", 
zaloguj  si�, a po zalogowaniu kliknij na przycisku "Opcje". 
Dodatkowo, je�li u�ywasz routera D-Link 
DI-704, konieczna b�dzie 
aktualizacja firmware do wersji 2.75 lub nowszej. Wi�cej informacji
na temat, jak tego dokona�, znajdziesz na stronie pomocy firmy 
D-Link.

Je�li grasz korzystaj�c z routera D-Link DI-604, konieczne b�dzie
dodanie dw�ch linii do pliku options.ini. Plik ten znajduje si� 
w Twoim folderze "My Battle for Middle-earth Files". Aby wej�� do
tego folderu, zapoznaj si� z sekcjami pliku czytajto po�wi�conymi
ustawieniom oraz zapisywaniu gier. Aby edytowa� plik options.ini,
kliknij na nim prawym przyciskiem i wybierz opcj� "Edytuj". 
Na samym dole pliku dodaj dwie linie:

   FirewallBehavior = 19
   FirewallPortAllocationDelta = 2

Kiedy wpiszesz te linie, zamknij plik options.ini. Pojawi si� pytanie,
czy chcesz zapisa� zmiany - wybierz "Tak". Po wprowadzeniu zmian do 
pliku options.ini, nie powinno si� od�wie�a� NATu. Wi�cej informacji
na ten temat mo�na znale�� w tym dokumencie, na ko�cu sekcji 
po�wi�conej ustawieniom NAT/Zapory po��cze� internetowych.

Warto zauwa�y�, �e je�li z pojedynczego routera D-Link DI-604 
korzysta wielu graczy, mog� wyst�pi� problemy w czasie pr�b
do��czenia do gry sieciowej.

Warto r�wnie� zauwa�y�, �e w przypadku router�w Belkin i USRobotics 
wyst�puj� pewne nierozwi�zane jak dot�d problemy, lecz dzieje si� to
jedynie podczas pr�b nawi�zania po��czenia mi�dzy nimi w szybkich 
meczach.

Je�li grasz w Bitw� o �r�dziemie(tm) u�ywaj� programowej zapory 
internetowej, konieczne b�dzie dodanie pliku wykonywalnego gry do
listy wyj�tk�w lub program�w dozwolonych. W zaporze trzeba b�dzie 
poda� pe�n� �cie�k� dost�pu do pliku wykonywalnego gry. Plik ten
zwykle znajduje si� tutaj: "C:\Program Files\
EA Games\Bitwa o �r�dziemie(tm)\lotrbfme.exe". Konieczne
b�dzie r�wnie� dodanie do tej listy pliku "game.dat", kt�ry 
znajduje si� w tym samym katalogu co plik wykonywalny*.

Je�li w czasie gry korzystasz z zapory, w��czenie przekierowywania 
port�w nie powinno by� konieczne. Gra Bitwa o �r�dziemie(tm) 
powinna dzia�a� prawid�owo przy wi�kszo�ci osobistych zap�r po��cze�
internetowych.

Je�li od czasu ostatniej gry zmienione zosta�y ustawienia routera, 
trzeba klikn�� na przycisku "Od�wie� NAT" w opcjach sieciowych gry.
Aby wej�� tam z g��wnego menu, kliknij na przycisku "Gra 
wieloosobowa", nast�pnie na przycisku "Sie�", zaloguj si�, a po 
zalogowaniu kliknij na przycisku "Opcje".

Bitwa o �r�dziemie(tm) u�ywa port�w UDP 8088-28088.

Je�li masz problemy z nawi�zaniem po��czenia, zamknij gr�, usu� plik 
options.ini z folderu "My Battle for Middle-earth Files", uruchom gr� 
ponownie i kliknij na przycisku "Od�wie� NAT" w menu opcji gry sieciowej. 

Je�li korzystasz z modemu kablowego i routera, skontaktuj si� z dostawc�,
aby usun�� zapor� modemu.


==================================================================
*Uprawnienia administratora
==================================================================

Prosimy pami�ta�, �e dodanie gry Bitwa o �r�dziemie(tm) do listy 
wyj�tk�w programowej zapory internetowej mo�e by� niemo�liwe, je�li
nie jeste� zalogowany jako administrator systemu. Konieczne b�dzie
utworzenie konta, kt�re posiada uprawnienia administratora lub 
doda� istniej�ce konto do listy administrator�w. Mo�na tego dokona�
w nast�puj�cy spos�b:

  1) Wyloguj si� z systemu i zaloguj na konto administratora.
  1) Po zalogowaniu, kliknij na przycisku "Start" na pasku zada� 
     Windows.
  2) Wybierz "Ustawienia".
  3) Wybierz "Panel sterowania".
  4) Wybierz "Konta u�ytkownik�w".
  5) Wybierz zak�adk� "U�ytkownicy".
  6) W cz�ci "U�ytkownicy tego komputera:" kliknij na swojej nazwie
     konta.
  7) Kliknij na "W�asno�ci".
  8) Wybierz zak�adk� "Cz�onkostwo grupy".
  9) Wybierz przycisk "Inne".
  10) Z rozwijanego menu wybierz "Administratorzy".
  11) Kliknij na "Zastosuj", a nast�pnie na "OK".


==================================================================
Tw�rcy
==================================================================

Gra W�adca Pier�cieni(tm), Bitwa o �r�dziemie(tm) wykorzystuje 
technik� dekodowania MP3. Autorami / w�a�cicielami tego formatu MP3 
s� Fraunhofer i Thomson.

