==================================================================
Legal Information
==================================================================

THIS SOFTWARE IS PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
WHETHER EXPRESS OR IMPLIED, INCLUDING WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE WHICH ARE
HEREBY DISCLAIMED. IN NO EVENT WILL ELECTRONIC ARTS BE LIABLE FOR
ANY SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES RESULTING FROM
POSSESSION, USE, OR MALFUNCTION OF THIS ELECTRONIC ARTS SOFTWARE
PRODUCT.

SOME STATES DO NOT ALLOW LIMITATIONS AS TO HOW LONG AN IMPLIED
WARRANTY LASTS AND/OR EXCLUSIONS OR LIMITATIONS OF INCIDENTAL OR
CONSEQUENTIAL DAMAGES SO THE ABOVE LIMITATIONS AND/OR EXCLUSIONS
OF LIABILITY MAY NOT APPLY TO YOU. THIS WARRANTY GIVES YOU
SPECIFIC RIGHTS, AND YOU MAY ALSO HAVE OTHER RIGHTS WHICH VARY
FROM STATE TO STATE.

ELECTRONIC ARTS RESERVES THE RIGHT TO MAKE IMPROVEMENTS IN THIS
FILE AND TO THE SOFTWARE AT ANY TIME AND WITHOUT NOTICE.
THIS FILE AND THE SOFTWARE HEREWITH IS COPYRIGHTED. ALL RIGHTS ARE
RESERVED. NO PART OF THIS FILE OR THE SOFTWARE MAY BE COPIED,
REPRODUCED, TRANSLATED, OR REDUCED TO ANY ELECTRONIC MEDIUM OR 
MACHINE-READABLE FORM WITHOUT THE PRIOR WRITTEN CONSENT OF 
ELECTRONIC ARTS.


==================================================================
Electronic Arts Copyright
==================================================================

Software and Documentation � 2004 Electronic Arts Inc.

ALL RIGHTS RESERVED.


==================================================================
The Lord of the Rings(tm), The Battle for Middle-earth(tm) 
Version 1.0 Readme File

November 12, 2004
==================================================================

Thank you for purchasing The Battle for Middle-earth(tm). This
readme file contains information that will help you begin your
journey across Middle-earth.


==================================================================
System Requirements
==================================================================

The Battle for Middle-earth(tm) requires DirectX 9.0b To
install DirectX 9.0b or later, visit:

  http://www.microsoft.com/windows/directx/default.aspx

REQUIRED SYSTEM SPEC:

  - Windows XP or Windows 2000
  - 1.3 GHz Intel Pentium IV or AMD Athlon processor
  - 256 MB RAM
  - Nvidia GeForce2 or equivalent video card with 32 MB RAM
  - DirectX 9.0b compatible sound card
  - 4 GB available hard disk space

Please note that while you will be able to play The Battle for 
Middle-earth(tm) with the specifications above, play experience can 
benefit from additional RAM, above the required 256 MB.

Nvidia GeForce2 MX is not supported. 


==================================================================
Installation Instructions
==================================================================

Before running The Battle for Middle-earth(tm), insure you have
the latest drivers for your video and audio cards installed. You
can download the appropriate drivers from your video and audio
card manufacturer's Web site.

Avoid running any other programs in the background, as this may
conflict with the game. This includes any virus-protection program 
that you have running, which can hinder performance.

To install The Battle for Middle-earth(tm):

  1) Insert the primary The Battle for Middle-earth(tm)
     installation disc into your DVD-ROM drive.

     If the disc does not automatically launch when inserted in the 
     drive, you may have this feature disabled. To enable AutoPlay, 
     please consult your Windows user manual.

     If you are unable or unwilling to activate AutoPlay, please
     browse to the location of The Battle for Middle-earth(tm) 
     DVD-ROM using Windows Explorer, and double
     mouse-click the "Setup" application in the root directory.

  2) Follow the instructions that appear to install The Battle for 
     Middle-earth(tm).

  3) Please note that you must register The Battle for Middle-earth(tm)
     when prompted to do so during the installation process in order
     to play the game online. You can register the game at a later
     time by clicking the "Register" button in the game's Online 
     menu.

If you are having difficulties with the CD key provided with The 
Battle for Middle-earth(tm), or require any other type of technical 
support, please click on the "Technical Support" button on the game's 
AutoPlay menu. You can also visit the official product Web site below 
and select the "Support" option. You will need to register your 
product for full customer support.

http://www.eagames.com/official/lordoftherings/thebattleformiddleearth/us/


==================================================================
Uninstalling The Battle for Middle-earth(tm)
==================================================================

  To uninstall The Battle for Middle-earth(tm):

  1) Bring up your Start Menu.
  2) Choose "Settings".
  3) Choose "Control Panel".
  4) Choose "Add/Remove Programs".
  5) Highlight "The Battle for Middle-earth(tm)" and click the
     "Change/Remove" button below its name.
  6) Follow the instructions that appear to uninstall.




==================================================================
Windows 2000
==================================================================

If you are attempting to run The Battle for Middle-earth(tm) on a 
Windows 2000 machine, you will need to make sure that you have 
downloaded Microsoft Service Pack 4 as well as all the latest updates. 
You can download these from Microsoft's Web site. 


==================================================================
Troubleshooting
==================================================================

An outdated video or sound driver can lead to slow and choppy 
gameplay, or in some cases can prevent the game from running at
all. To ensure an enjoyable experience with The Battle for Middle-
earth(tm), be sure that you have the most recent video and sound
drivers installed. These drivers are typically available for
download from your system or hardware manufacturer�s Web site. If
you are not sure what type of video or sound card you have, or you
need assistance with updating the drivers on your system, please
refer to the documentation that came with your system or peripheral.


==================================================================
Save Games and Settings
==================================================================

The Battle for Middle-earth(tm) save files and other user-related 
settings are stored in the "My Battle for Middle-earth Files"
folder, which can typically be accessed by going to your C:\ drive, 
opening the "Documents and Settings" folder, opening the folder 
labeled with your Windows user name, and opening the "Application
Data" folder. If you don't see an "Application Data" folder here, 
do the following:

  1) Bring up any Windows Explorer window by holding down the Windows
     key on your keyboard and pressing "E".
  2) Click on "Tools" from the top menu.
  3) Choose "Folder Options" from the drop down menu.
  4) Choose the "View" tab.
  5) Click on the "Show hidden files and folders" radio button under
     "Advanced settings".
  6) Click on "Apply" and then "OK".
  7) The "Application Data" folder should now be visible.

Please note that there is only one AUTOSAVE file for The Battle for 
Middle-earth(tm). If you start either campaign, and then switch to 
the other campaign, your AUTOSAVE file from the previous campaign 
will get overwritten.


==================================================================
Command Line Parameters
==================================================================

The Battle for Middle-earth(tm) supports several command line 
parameters that will give you increased flexibility in the way 
that you run the game. To set a command line parameter, you need to 
do the following:

  1) Go to the location of the game's executable. This is typically 
     found in C:\Program Files\EA Games\The Battle for Middle-earth(tm)\
     lotrbfme.exe.
  2) Right-click on the lotrbfme.exe icon and select "Create Shortcut".
  3) This will create a shortcut of the executable within the same 
     folder. This will typically be called "Shortcut to lotrbfme.exe".
  4) Right-click on this new shortcut and select "Properties".
  5) Add a space to the end of the string in the "Target:" field, 
     followed by a "-" and then the command line parameter.

The following command line parameters are supported by The Battle for 
Middle-earth(tm):

     -noshellmap (Launches the game without the Barad-dur looping 
                  background)
     -mod (Allows modders to load custom assets into the game. Simply 
           add the "-mod" command line after the lotrbfme.exe target 
           path, followed by the name of any user-created .big file or 
           user-created directory. The correct path would look like this: 
           "C:\Program Files\EA Games\The Battle for Middle-earth(tm)\
           lotrbfme.exe" -mod MyLOTRMod.big. All user-created content 
           must be placed in the "My Battle for Middle-earth Files" 
           folder)
     -noaudio (Launches the game without any audio)
     -xres (Forces the game to a specific horizontal resolution)
     -yres (Forces the game to a specific vertical resolution)


==================================================================
Known Issues
==================================================================

On some video cards that are running dual-monitor setups, you may
encounter visual distortions on the screen. If you encounter this
issue, go to the game's "Main Menu" and click on "Options". Choose 
a different resolution under "Display Options" and then click on 
"Accept Changes". This should correct the problem. If you choose, 
you can switch back to your original resolution.

If you play the game on an ATI Radeon 7500 video card, you may 
encounter subtle multi-colored rainbow effects in-game. This 
occurrence should be infrequent and should not compromise gameplay 
in any way. If you encounter this problem, please visit the ATI 
Web site to ensure that you are using up-to-date drivers.

You may encounter audio problems with some Creative Labs Audigy 
sound cards if you select the EAX3 option in the Options menu. 
Make sure you update your Audigy with the latest drivers.

If you choose to select the EAX3 option, make sure that you have 
the proper speaker type selected in Windows. To do so, do the 
following:

  1) In Windows, click on "Start".
  2) Choose "Settings".
  3) Choose "Control Panel".
  4) Choose "Sounds and Audio Devices Properties".
  5) Select the "Volume" tab.
  6) Under "Speaker settings" click on "Advanced...".
  7) Select the "Speakers".
  8) From the "Speaker setup:" pulldown menu, choose your applicable 
     speaker setup.
  9) Click on "Apply" and then "OK".

If you have your screensaver enabled and it is activated while the 
game is loading any mission, the game will be minimized to your Windows 
taskbar, which is typically located at the bottom of your Windows 
desktop. If this happens, deactivate the screensaver and maximize the 
game by clicking on the "Lord of the Rings The Battle for Middle-earth" 
button on the taskbar.

Users running Windows 2000 may experience a slow transition back to 
the Main Menu after exiting a skirmish game with six or more 
AI-controlled opponents. If you experience such a slowdown, you can 
try addressing it by changing your "Detail" setting to Low in the 
game's Options Menu.  


==================================================================
Multiplayer Information
==================================================================

When attempting to play The Battle for Middle-earth(tm) online, 
please note that the minimum character length for your "Nickname"
is four characters. If you attempt to login with a nickname that
is three characters or less, the "Login" button will remain inactive. 
Please also note that the maximum allowable Nickname length is 15 
characters, and you cannot start your Nickname with any non-alphabet 
character.

Your multiplayer stats are automatically recorded and submitted to
a 1v1 or 2v2 ladder only when you play Quick Match. Quick Match will
find a user within a certain percentage of your rank and ping to 
play against. You can click the "Widen Search" button to remove rank 
and ping restrictions.


==================================================================
NAT/Firewall
==================================================================

If you are playing The Battle for Middle-earth(tm) behind a USRobotics 
router, you will need to upgrade its firmware to the latest version, 
v2.7 or later. See the USRobotics support Web site for details on 
how to do so.

If you are playing the game behind a D-Link router, you must check the 
"Send Delay" box in the Online Options menu for The Battle for 
Middle-earth(tm). To get here from the Main Menu, click on the 
Multiplayer button, click the Online button, login as you regularly 
would, and then click on Options after you have successfully logged on. 
Additionally, if you are playing behind a D-Link DI-704, you will need to 
upgrade its firmware to version 2.75 build 3 or later. See the D-Link 
support Web site for details on how to do so. 

If you are playing the game behind a D-Link DI-604, you will need to add 
two lines to the options.ini file. The options.ini is located in your 
"My Battle for Middle-earth Files" folder. To access this folder, please 
check the Save Games and Settings section of this readme above. To edit 
the options.ini file, right-click on it with your mouse cursor, and 
select "Edit". At the bottom of the file, add the following lines:

   FirewallBehavior = 19
   FirewallPortAllocationDelta = 2

When you've finished adding these lines, close the options.ini file. 
Windows will prompt you whether or not you wish to save--choose "Yes". 
If you update your Options.ini file, you should not refresh NAT. More 
details about refresh NAT can be found at the bottom of the NAT/Firewall 
portion of the readme.

Please note that if you have multiple players behind a single D-Link 
DI-604 connection, you may experience difficulty connecting.

It should also be noted that Belkin and USRobotics routers have 
irresolvable issues, but only when attempting to connect to each other 
via Quick Match. Additionally, Belkin and USRobotics routers will 
also have difficulty connecting to D-Link DI-604 routers via Quick 
Match.

If you are playing The Battle for Middle-earth(tm) behind a software 
firewall, you will need to add the game's executable to the "Exceptions 
or Allowed Programs" list provided by your software firewall. You will 
need to provide your software firewall with the actual path to the 
game's executable. This file is typically located in "C:\Program Files\
EA Games\The Battle for Middle-earth(tm)\lotrbfme.exe". You will also 
need to add the "game.dat" file, which is located in the same directory 
listed above, to this exceptions list*.

There should be no need for you to use Port Forwarding/Port Triggering 
to play the game behind your firewall. The Battle for Middle-earth(tm) 
should work successfully behind most personal firewalls.

If you have changed your router settings since the last time you played 
The Battle for Middle-earth(tm), you will need to click on the "Refresh 
NAT" button in the game's Online Options menu. To get here from the Main 
Menu, click on the Multiplayer button, click the Online button, login 
as you regularly would, and then click on Options after you have 
successfully logged on.

The Battle for Middle-earth(tm) uses UDP ports 8088-28088.

If you are experiencing general NAT connection difficulties, exit 
the game, delete the options.ini file in your "My Battle for 
Middle-earth Files" folder, restart the game, and click the 
"Refresh NAT" button in the Online Options menu. 

Additionally, if you are using a combination of a cable modem and 
a router, you should contact your ISP/cable provider in order to 
disable your modem's built-in firewall.


==================================================================
*Administrator Privileges
==================================================================

Please note that you may not be able to add The Battle for Middle-
earth(tm) to your software firewall's exceptions list if you are not 
logged in as an administrator in Windows. You will either need to do so 
from an account that has administrator rights, or add your existing 
account to the administrators list. To do so, do the following:

  1) Log out of Windows and log in with an Administrator account.
  1) Once logged in, click on "Start" in the Windows taskbar.
  2) Choose "Settings".
  3) Choose "Control Panel".
  4) Choose "User Accounts".
  5) Select the "Users" tab.
  6) Under "Users for this computer:" click on your account name.
  7) Click on "Properties".
  8) Select the "Group Membership" tab.
  9) Select the "Other" radio button.
  10) From the pulldown menu, select "Administrators".
  11) Click on "Apply" and then "OK".


==================================================================
Credits
==================================================================

The Lord of the Rings(tm), The Battle for Middle-earth(tm) uses 
MP3 decoding technology. Fraunhofer and Thomson are the creators / 
owners of this MP3 format.