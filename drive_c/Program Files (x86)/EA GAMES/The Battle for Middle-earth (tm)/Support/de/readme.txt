==================================================================
Legal Information
==================================================================

THIS SOFTWARE IS PROVIDED AS IS WITHOUT WARRANTY OF ANY KIND,
WHETHER EXPRESS OR IMPLIED, INCLUDING WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE WHICH ARE
HEREBY DISCLAIMED. IN NO EVENT WILL ELECTRONIC ARTS BE LIABLE FOR
ANY SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES RESULTING FROM
POSSESSION, USE, OR MALFUNCTION OF THIS ELECTRONIC ARTS SOFTWARE
PRODUCT.

SOME STATES DO NOT ALLOW LIMITATIONS AS TO HOW LONG AN IMPLIED
WARRANTY LASTS AND/OR EXCLUSIONS OR LIMITATIONS OF INCIDENTAL OR
CONSEQUENTIAL DAMAGES SO THE ABOVE LIMITATIONS AND/OR EXCLUSIONS
OF LIABILITY MAY NOT APPLY TO YOU. THIS WARRANTY GIVES YOU
SPECIFIC RIGHTS, AND YOU MAY ALSO HAVE OTHER RIGHTS WHICH VARY
FROM STATE TO STATE.

ELECTRONIC ARTS RESERVES THE RIGHT TO MAKE IMPROVEMENTS IN THIS
FILE AND TO THE SOFTWARE AT ANY TIME AND WITHOUT NOTICE.
THIS FILE AND THE SOFTWARE HEREWITH IS COPYRIGHTED. ALL RIGHTS ARE
RESERVED. NO PART OF THIS FILE OR THE SOFTWARE MAY BE COPIED,
REPRODUCED, TRANSLATED, OR REDUCED TO ANY ELECTRONIC MEDIUM OR 
MACHINE-READABLE FORM WITHOUT THE PRIOR WRITTEN CONSENT OF 
ELECTRONIC ARTS.


==================================================================
Electronic Arts Copyright
==================================================================

Software and Documentation (c) 2004 Electronic Arts Inc.

ALL RIGHTS RESERVED.


==================================================================
Der Herr der Ringe(tm), Die Schlacht um Mittelerde(tm) 
Version 1.0 Readme-Datei

11. November 2004
==================================================================

Danke, dass du dich f�r Die Schlacht um Mittelerde(tm) entschieden
hast. Diese Datei enth�lt Informationen, die dir die ersten Schritte 
in Mittelerde erleichtern sollen.


==================================================================
Systemanforderungen
==================================================================

Die Schlacht um Mittelerde(tm) ben�tigt DirectX 9.0b. Wenn du 
DirectX 9.0b oder h�her installieren m�chtest, besuche bitte 
folgende Website:

  http://www.microsoft.com/windows/directx/default.aspx

MINDESTVORAUSSETZUNGEN:

  - Windows XP oder Windows 2000
  - 1.3 GHz Intel Pentium IV- oder AMD Athlon-Prozessor
  - 256 MB RAM
  - Nvidia GeForce 2 oder gleichwertige Grafikkarte mit 32 MB RAM
  - DirectX 9.0b-kompatible Soundkarte
  - 4 GB verf�gbarer Festplattenspeicher

Hinweis: Die Schlacht um Mittelerde(tm) kann mit den oben genannten
Systemvoraussetzungen ausgef�hrt werden. Allerdings ist die 
Spielerfahrung auf einem System mit mehr als 256 MB Arbeitsspeicher
besser.

Nvidia GeForce2 MX wird nicht unterst�tzt.

==================================================================
Installationsanweisungen
==================================================================

Installiere vor der Ausf�hrung von Die Schlacht um Mittelerde(tm)
aktuelle Grafik- und Soundtreiber. Aktuelle Treiber findest du auf 
der Website des Herstellers deiner Grafik- bzw. Soundkarte. 

F�hre keine Programme im Hintergrund aus, da diese Die Schlacht 
um Mittelerde(tm) st�ren k�nnten. Auch Antiviren-Programme k�nnen 
sich negativ auf die Leistung des Spiels auswirken.

Installation von Die Schlacht um Mittelerde(tm):

  1) Lege die Die Schlacht um Mittelerde(tm)-DVD in dein
     DVD-Rom-Laufwerk ein.

     Wird Die Schlacht um Mittelerde(tm) nach dem Einlegen der 
     DVD nicht automatisch gestartet, ist die Autoplay-Funktion 
     deines DVD-ROM-Laufwerks deaktiviert. Weitere Informationen zur 
     Aktivierung der Funktion findest du im Handbuch zu Windows.

     M�chtest du das Spiel manuell installieren, rufe im Explorer
     dein DVD-ROM-Laufwerk auf und doppelklicke im Stammverzeichnis 
     der DVD auf die Datei "Setup.exe".

  2) Folge den Bildschirmanweisungen, um Die Schlacht um Mittelerde(tm)
     zu installieren.

  3) M�chtest du Die Schlacht um Mittelerde(tm) auch online spielen, musst
     du dich registrieren lassen, wenn du dazu aufgefordert wirst.
     Nat�rlich kannst du die Registrierung auch nach der Installation
     des Spiels durchf�hren. Klicke dazu im Online-Men� auf den Button
     "Registrieren".

Treten mit dem Spiel oder der Seriennummer deiner Version von Die Schlacht
um Mittelerde(tm) Probleme auf, besuche bitte unten stehende Website und 
w�hle die Option "Support". Wenn du unseren Kundendienst in Anspruch nehmen
m�chtest, musst du das Spiel registrieren.

http://www.eagames.com/official/lordoftherings/thebattleformiddleearth/de/



==================================================================
Die Schlacht um Mittelerde(tm) deinstallieren
==================================================================

  Wenn du Die Schlacht um Mittelerde(tm) deinstallieren m�chtest:

  1) Rufe das Startmen� auf.
  2) W�hle "Einstellungen".
  3) W�hle "Systemsteuerung".
  4) W�hle "Software".
  5) Markiere "Die Schlacht um Mittelerde(tm)" und klicke auf den
     Entfernen-Button.
  6) Folge den Bildschirmanweisungen, um die Deinstallation abzuschlie�en.



==================================================================
Windows 2000
==================================================================

Wenn du versuchst, Die Schlacht um Mittelerde(tm) auf einem Rechner zu
installieren, auf dem Windows 2000 als Betriebssystem vorhanden ist, beachte
bitte, dass das Service Pack 4 und alle weiteren Updates installiert sind.
Diese kannst du von der Microsoft-Webseite herunterladen.


==================================================================
Fehlerbehebung 
==================================================================

Veraltete Grafik- oder Soundtreiber wirken sich negativ auf die
Leistung des Spiels aus. Installiere daher vor der Ausf�hrung des
Spiels aktuelle Grafik- und Soundtreiber. Aktuelle Treiber findest
du auf der Website des Herstellers deiner Sound- bzw. Grafikkarte.
Wenn du nicht wei�t, welche Karte in deinem System installiert ist,
oder du bei der Aktualisierung der Treiber Hilfe brauchst, wirf 
bitte einen Blick in das Handbuch der jeweiligen Karte. 


==================================================================
Spiele und Einstellungen speichern
==================================================================

Die Speicherdateien und andere Benutzereinstellungen von Die Schlacht
um Mittelerde(tm) werden im Ordner " Meine Die Schlacht um Mittelerde-Dateien"
gespeichert. Diesen findest du normalerweise unter C:\Dokumente und
Einstellungen. �ffne hier den Ordner mit deinem Benutzernamen und
doppelklicke auf "Anwendungsdaten". Wird 
kein entsprechender Ordner angezeigt, gehe folgenderma�en vor:

  1) Halte die Windows-Taste deiner Tastatur gedr�ckt und rufe mit 
     der Taste "E" ein Explorer-Fenster auf.
  2) Klicke in der Men�leiste auf "Extras".
  3) W�hle im nun erscheinenden Men� den Eintrag "Ordneroptionen".
  4) W�hle die Registerkarte "Ansicht".
  5) Klicke unter "Erweiterte Einstellungen" auf den Eintrag 
     "Alle Dateien und Ordner anzeigen".
  6) Klicke auf "�bernehmen" und anschlie�end auf "OK".
  7) Nun m�sste der Ordner "Anwendungsdaten" angezeigt werden.

Die Schlacht um Mittelerde(tm) wird automatisch gespeichert. 
Wechselst du w�hrend des Spiels die Kampagne, wird die Autosave-Datei
der alten Kampagne �berschrieben.

==================================================================
Befehlszeilen-Parameter
==================================================================

Die Schlacht um Mittelerde(tm) unterst�tzt verschiedene Befehlszeilen-
Parameter, mit denen du das Spiels anpassen kannst.
M�chtest du einen Befehlszeilen-Parameter setzen, gehe folgenderma�en
vor:

  1) �ffne den Ordner, in dem die ausf�hrbare Datei des Spiels gespeichert
     ist (normalerweise C:\Programme\EA Games\Die Schlacht um
     Mittelerde(tm)\lotrbfme.exe.
  2) Rechtsklicke auf die Datei "lotrbfme.exe" und w�hle "Verkn�pfung 
     erstellen".
  3) Dadurch wird im gleichen Ordner eine Verkn�pfung mit der ausf�hrbaren
     Datei erstellt. Diese tr�gt den Namen "Verkn�pfung mit lotrbfme.exe".
  4) Rechtsklicke auf die Verkn�pfung und w�hle "Eigenschaften".
  5) Gib im Eingabefeld "Ziel:" hinter dem Eintrag ein Leerzeichen, einen 
     Bindestrich ("-") und den gew�nschten Parameter ein.

Folgende Parameter werden von Die Schlacht um Mittelerde(tm) unterst�tzt:

     -noshellmap (Startet das Spiel ohne den Barad-Dur-Hintergrund.)
     -mod (Erm�glicht Moddern, eigene Dateien in das Spiel zu laden. Gib 
           dazu "-mod" und den Namen der gew�nschten .big-Datei bzw. 
           des gew�nschten Ordners hinter dem Befehlszeileneintrag 
           (lotrbfme.exe) ein.
           Der korrekte Pfad sieht in diesem Fall etwa so aus: 
           "C:\Programme\EA Games\Die Schlacht um Mittelerde(tm)\
           lotrbfme.exe" -mod MeinLOTRMod.big. Alle eignen Inhalte
           m�ssen im Ordner "Meine Die Schlacht um Mittelerde-Dateien"
           platziert werden.)
     -noaudio (Starte das Spiel ohne Soundausgabe.)
     -xres (Definiere die horizontale Aufl�sung des Spiels.)
     -yres (Definiere die vertikale Aufl�sung des Spiels.)

==================================================================
Bekannte Probleme
==================================================================

Auf Systemen mit zwei Monitoren treten m�glicherweise Grafikprobleme
auf. Wechsle in diesem Fall in das Hauptmen� des Spiels, klicke auf
"Optionen" und w�hle im Bildschirm "Anzeigeoptionen" eine andere 
Aufl�sung aus. Best�tige deine Entscheidung anschlie�end mit einem
Klick auf "�nderungen �bernehmen". Nat�rlich kannst du auch die
urspr�ngliche Aufl�sung wiederherstellen.

Wenn du mit einer Radeon 7500-Grafikkarte von ATI spielst, treten
w�hrend des Spiels mehrfarbige Regenbogeneffekte auf. Diese wirken
sich allerdings nicht auf das Gameplay aus. Sollte dieses Problem
auftreten, besuche die Website von ATI und lade aktuelle
Grafiktreiber herunter.

Mit einigen Audigy-Soundkarten von Creative Labs k�nnen Probleme 
auftreten, wenn du im Optionen-Men� des Spiels die Option "EAX3" 
aktivierst. Installiere in diesem Fall aktuelle Treiber.

Wenn du die Option EAX3 aktivierst, musst du unter Windows die korrekten
Lautsprecher ausw�hlen. Gehe dazu folgenderma�en vor:

  1) Klicke auf "Start".
  2) W�hle "Einstellungen".
  3) W�hle "Systemsteuerung".
  4) W�hle "Sounds und Multimedia".
  5) W�hle die Registerkarte "Lautst�rke".
  6) Klicke unter "Lautsprechereinstellungen" auf "Erweitert ...".
  7) W�hle "Lautsprecher".
  8) W�hle in der Liste "Lautsprecher-Setup:" die gew�nschten 
	Lautsprecher aus.
  9) Klicke auf "�bernehmen" und anschlie�end auf "OK".

Wird w�hrend des Ladens einer Mission ein Bildschirmschoner aktiviert,
wird das Spiel minimiert. Deaktiviere in diesem Fall den Bildschirmschoner
und klicke in der Taskleiste (am unteren Bildschirmrand) auf 
"Der Herr der Ringe, Die Schlacht um Mittelerde".

Unter Windows 2000 dauert es nach einem Gefecht mit sechs oder 
mehr KI-Gegnern m�glicherweise relativ lange, bis das Hauptmen� 
eingeblendet wird. Tritt dieses Problem auf, solltest du die 
Detaileinstellungen des Spiels im Optionen-Men� senken.  

==================================================================
Multiplayer-Informationen
==================================================================

Spielst du Die Schlacht um Mittelerde(tm) online, brauchst du einen 
mindestens 4 Zeichen langen Spielernamen. Ist dein Spielername zu 
kurz, kannst du dich nicht einloggen.
Denke au�erdem daran, dass der Spielername maximal 15 Zeichen lang 
sein darf und das erste Zeichen ein Buchstabe sein muss.

Deine Multiplayer-Statistiken werden nur im QuickMatch-Modus an eine
entsprechende 1g1- oder 2g2-Rangliste gesendet. QuickMatch sucht 
automatisch geeignete Spieler, gegen die du antreten kannst. 
Klicke auf "Suche ausweiten", wenn du die Suchkriterien 
erweitern m�chtest.


==================================================================
NAT/Firewall
==================================================================

M�chtest du Die Schlacht um Mittelerde(tm) �ber einen Router von USRobotics 
spielen, brauchst du dessen aktuelle Firmware (v2.7 oder h�her). Weitere 
Informationen dazu findest du auf der Website von USRobotics.

Hast du einen D-Link-Router, musst du das Kontrollk�stchen "Delay 
senden" im Men� "Online-Optionen" aktivieren, damit du Die Schlacht um 
Mittelerde(tm) spielen kannst. Klicke dazu im Multiplayer-Men� auf den 
Button "Online", logge dich wie gewohnt ein, und klicke nach dem Log-in
auf "Optionen". 
Spielst du �ber einen D-Link DI-704, musst du dar�ber hinaus die Firmware des
Routers auf Version 2.75 (Build 3) oder h�her aktualisieren. Weitere
Informationen dazu findest du auf der Website von D-Link.

Hast du einen D-Link DI-604-Router, musst du zwei Zeilen in die 
options.ini-Datei des Spiels eintragen. Du findest die Datei im Ordner
"Meine Die Schlacht um Mittelerde-Dateien". Wenn du nicht wei�t, wo
sich dieser Ordner befindet, lies bitte den Abschnitt "Spiele und 
Einstellungen speichern". M�chtest du die options.ini-Datei bearbeiten,
markiere sie mit einem Rechtsklick und w�hle im nun erscheinenden
Men� "Bearbeiten". F�ge am Ende der Datei folgende Zeilen ein:

   FirewallBehavior = 19
   FirewallPortAllocationDelta = 2

Hast du die Zeilen eingef�gt, kannst du die options.ini-Datei schlie�en. 
W�hle im nun erscheinenden Fenster "Ja", um die �nderung zu best�tigen.
Wenn du deine Options.ini-Datei ge�ndert hast, solltest du deine NAT nicht
aktualisieren. Weitere Informationen dazu findest du am Ende dieses 
Abschnitts.

Spielen mehrere Spieler �ber eine einzige D-Link DI-604-Verbindung, treten
m�glicherweise Verbindungsprobleme auf.

Hinweis: Die Verbindung eines Belkin-Routers mit einem Ger�t von 
USRobotics (und umgekehrt) ist im QuickPlay-Modus nicht m�glich.
Zus�tzlich haben Belkin- und USRobotics-Router Schwierigkeiten, im QuickPlay-Modus
eine Verbindung zum D-Link DI-604-Router aufzubauen.

Hast du eine Software-Firewall installiert, musst du die ausf�hrbare
Datei des Spiels sowie die Datei "game.dat" in die Liste der 
Internet-Anwendungen* einf�gen. Gib dazu den Pfad der ausf�hrbaren Datei ein
(standardm��ig C:\Programme\EA Games\Die Schlacht um Mittelerde(tm)\lotrbfme.exe).
Die Game.dat-Datei findest du im gleichen Ordner.

Bei der Ausf�hrung des Spiels d�rften jetzt keine Probleme mehr auftreten.
Weitere Einstellungen sind in der Regel nicht erforderlich.

Hast du die Router-Einstellungen ge�ndert, klicke im Men� "Online-Optionen"
auf den Button "NAT aktualisieren". Klicke dazu im Multiplayer-Men� auf den 
Button "Online", logge dich wie gewohnt ein, und klicke nach dem Log-in
auf "Optionen".

Die Schlacht um Mittelerde(tm) benutzt die UDP-Ports 8088-28088.

Wenn du generelle NAT-Verbindungsschwierigkeiten hast, verlasse das Spiel, l�sche
die Datei options.ini aus dem Ordner "Meine Die Schlacht um Mittelerde-Dateien", starte das Spiel
erneut und klicke auf den Button "NAT aktualisieren" im Men� "Online-Optionen".

Falls du zus�tzlich noch eine Kombination aus Kabelmodem und Router benutzt, solltest
du deinen ISP/Kabel-Provider kontaktieren, um die eingebaute Firewall in deinem ROuter
zu deaktivieren.


==================================================================
*Administratorrechte
==================================================================

Du musst unter Windows als Administrator angemeldet sein, damit du 
die Einstellungen deiner Firewall bearbeiten kannst. Hast du kein Konto
mit Administratorrechten, musst du dein Konto in die Liste der 
Administratoren aufnehmen. Gehe dazu folgenderma�en vor:

  1) Beende Windows und melde dich anschlie�end mit einem 
        Administrator-Konto an.
  2)  Klicke in der Taskleiste auf "Start".
  3)  W�hle "Einstellungen".
  4)  W�hle "Systemsteuerung".
  5)  W�hle "Benutzer und Kennw�rter".
  6)  W�hle die Registerkarte "Benutzer".
  7)  Klicke unter "Benutzer des Computers:" auf das gew�nschte Konto.
  8)  Klicke auf "Eigenschaften".
  9)  W�hle die Registerkarte "Mitgliedschaft in Gruppen".
  10) W�hle "Andere".
  11) W�hle in der Liste die Option "Administratoren" aus.
  12) Klicke auf "�bernehmen" und anschlie�end auf "OK".


==================================================================
Mitwirkende
==================================================================

The Lord of the Rings(tm), The Battle for Middle-earth(tm) uses 
MP3 decoding technology. Fraunhofer and Thomson are the creators / 
owners of this MP3 format.





